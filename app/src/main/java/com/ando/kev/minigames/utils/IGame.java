package com.ando.kev.minigames.utils;

/**
 * Created by kev on 13/08/17.
 */

public interface IGame {

    String name();
    boolean gameOver();
}
