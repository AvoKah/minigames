package com.ando.kev.minigames.games;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import com.ando.kev.minigames.R;
import com.ando.kev.minigames.utils.IGame;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;

public class TicTacToe extends AppCompatActivity implements IGame {

    Button b1, b2, b3, b4, b5, b6, b7, b8, b9;
    List<Button> buttons;

    boolean turn;
    String winner = null;

    @Override
    public String name() {
        return "Tic Tac Toe";
    }

    @Override
    public boolean gameOver() {
        return winner != null;
    }

    private void initialize()
    {
        buttons = new Vector<>();

        buttons.add(b1);
        buttons.add(b2);
        buttons.add(b3);
        buttons.add(b4);
        buttons.add(b5);
        buttons.add(b6);
        buttons.add(b7);
        buttons.add(b8);
        buttons.add(b9);
    }

    private Pair<Boolean, List<Button>> is_valid(int i, int j, int k)
    {
        if (!buttons.get(i).getText().toString().equals("")
                && buttons.get(i).getText().toString().equals(buttons.get(j).getText().toString())
                && buttons.get(i).getText().toString().equals(buttons.get(k).getText().toString()))
        {
            Button[] tmp = new Button[]{buttons.get(i), buttons.get(j), buttons.get(k)};
            return new Pair<>(true, Arrays.asList(tmp));
        }
        return new Pair<>(false, null);
    }

    private boolean isGameOver()
    {
        Pair<Boolean, List<Button>> pair;
        if ((pair = is_valid(0, 4, 8)).first || (pair = is_valid(2, 4, 6)).first) 
            winner = buttons.get(4).getText().toString();
        for (int i = 0; i < 9 && winner == null; i += 3)
            if ((pair = is_valid(i, i + 1, i + 2)).first)
                winner = buttons.get(i).getText().toString();
        for (int i = 0; i < 3 && winner == null; i++)
            if ((pair = is_valid(i, i + 3, i + 6)).first)
                winner = buttons.get(i).getText().toString();
        if (winner != null)
            Toast.makeText(this, "Winner player " + winner, Toast.LENGTH_SHORT).show();
        if (pair.first)
            for (Button b : pair.second)
                b.setBackgroundColor(Color.GREEN);

        return winner != null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tictactoe);

//        Intent in = getIntent();

        turn = false;

        b1 = (Button) findViewById(R.id.b1);
        b2 = (Button) findViewById(R.id.b2);
        b3 = (Button) findViewById(R.id.b3);
        b4 = (Button) findViewById(R.id.b4);
        b5 = (Button) findViewById(R.id.b5);
        b6 = (Button) findViewById(R.id.b6);
        b7 = (Button) findViewById(R.id.b7);
        b8 = (Button) findViewById(R.id.b8);
        b9 = (Button) findViewById(R.id.b9);

        initialize();
        
        for (final Button b : buttons) {
            if (gameOver())
                break;
            b.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    if (!gameOver() && b.getText().toString().equals("")) {
                        turn = !turn;
                        if (turn) {
                            b.setText("X");
                            v.setBackgroundColor(Color.WHITE);
                        }
                        else {
                            b.setText("O");
                            ((Button) v).setTextColor(Color.WHITE);
                            v.setBackgroundColor(Color.BLACK);
                        }
                        if (isGameOver())
                            return;
                    }
                }
            });
        }
    }
}
