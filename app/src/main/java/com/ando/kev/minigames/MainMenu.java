package com.ando.kev.minigames;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.ando.kev.minigames.games.TicTacToe;

public class MainMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
    }

    public void BtnClick(View view)
    {
        startActivity(new Intent(this, TicTacToe.class));
    }

    public void AddPlayer(View view)
    {

    }
}
